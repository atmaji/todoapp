import React from 'react';
import Modal from 'react-modal';
import { Button } from 'reactstrap';
import Icon from '@mdi/react';
import { mdiCloseCircleOutline } from '@mdi/js';

const FormModal = ({ isOpen = false, children, onToggle, title, onSubmit, width = '70%', onDelete }) => {
    const style = {
        overlay: { zIndex: '1150', background: 'rgba(0, 0, 0, 0.5)' },
        content: {
            borderRadius: '0',
            width: width,
            maxHeight: '100%',
            margin: '0 auto',
            left: '0',
            right: '0',
            overflow: 'visible',
            position: 'relative',
            border: 'none'
        }
    };
    return (
        <Modal isOpen={isOpen} style={style}>
            <div className="close-button" onClick={onToggle}>
                <Icon path={mdiCloseCircleOutline} size={1.2} color="#FFFFFF" />
            </div>
            <div className="header">
                <span className="title">{title}</span>
            </div>
            <form onSubmit={onSubmit}>
                <div className="body">
                    <div className="form">
                        {children}
                    </div>
                </div>
                <div className="footer d-flex justify-content-center">
                    <Button color="primary" type="submit">Submit</Button>
                    {/* <Button color="danger" className="ml-3" onClick={() => {onToggle}} type="submit">Delete</Button> */}
                    <Button color="secondary" onClick={onToggle} className="ml-3">Cancel</Button>
                </div>
            </form>
        </Modal>
    )
};

export default FormModal;