import {
    mdiDelete,
    mdiPencilOutline,
} from '@mdi/js';
import React from 'react';
import Icon from '@mdi/react';

const ActionButton = ({ text, icon, onClick }) => (
    <div style={{ cursor: 'pointer', marginBottom: '5px' }} onClick={onClick}>
        <Icon path={icon} size={.7} className="mr-1" color="#006FBA" /> {text}
    </div>
);

const DeleteButton = ({ onClick }) => <ActionButton text="Delete" icon={mdiDelete} onClick={onClick} />;

const UpdateButton = ({ onClick }) => <ActionButton text="Update" icon={mdiPencilOutline} onClick={onClick} />;

export default {
    DeleteButton,
    UpdateButton
}