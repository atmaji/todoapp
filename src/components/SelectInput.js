import React from 'react';
import Select from 'react-select';

const SelectInput = ({ input, meta, label, options, disabled = false }) => {

    const customStyles = {
        control: (base, state) => ({
            ...base,
            '&:hover': null,
            '&:active': {
                border: '2px solid #246EE9'
            },
            border: meta.touched && meta.error ? '2px solid red' : (state.isFocused ? '2px solid #246EE9' : '1px solid #555555'),
            boxShadow: 'none'
        }),
        indicatorSeparator: base => ({
            ...base,
            background: 'transparent'
        }),
    }

    return (
        <div className="form-group lov">
            <label className={`control-label mb-1`}>{label}</label>
            <Select
                isSearchable={false}
                styles={customStyles}
                onChange={input.onChange}
                value={input.value}
                options={options}
                onBlur={() => input.onBlur(input.value)}
                isDisabled={disabled} />
            {
                meta.touched &&
                meta.error &&
                <small className="help-block form-text" style={{ color: 'red' }}>{meta.error}</small>
            }
        </div>
    );
};

export default SelectInput;