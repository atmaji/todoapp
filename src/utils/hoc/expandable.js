import React, { Component } from 'react';

export default ComposedComponent => (
    class extends Component {
        _isMounted = false;

        state = {
            isOpen: false
        };

        componentDidMount() {
            this._isMounted = true;
        }

        componentWillUnmount() {
            this._isMounted = false;
        }

        onToggle = () => {
            if (!this.state.isOpen) {
                document.addEventListener('click', this.onOutisdeClick, false);
            } else {
                document.removeEventListener('click', this.onOutisdeClick, false);
            }


            this._isMounted && this.setState({
                isOpen: !this.state.isOpen
            });
        };

        onOutisdeClick = e => {
            if (!this.node) {
                return;
            }

            if (this.node.contains(e.target)) {
                return;
            }

            this.onToggle();
        }

        onSetWrapper = (e) => {
            this.node = e;
        }

        render() {
            return <ComposedComponent {...this} {...this.state} {...this.props} />;
        }
    }
);