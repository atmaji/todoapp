import axios from 'axios';

const getBaseUrl = "https://virtserver.swaggerhub.com/hanabyan/todo/1.0.0/to-do-list"

const get = () =>
    axios.get(getBaseUrl, {
        timeout: 10000,
        headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json'
        }
    });

const setupInterceptors = store => {
    axios.interceptors.request.use(config => {
        return config;
    });

    axios.interceptors.response.use(
        response => {
            const { data } = response;
            return data;
        },
        error => Promise.reject(error.toString())
    );
};

export default { get, setupInterceptors };