import React from 'react';
import ReactDOM from 'react-dom';
import AppContainer from 'features/app';
import 'assets/css/index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-table-6/react-table.css'
import 'assets/css/main.scss';
import { Provider } from 'react-redux';
import store from './store';
import Modal from 'react-modal';
import api from 'utils/api';

api.setupInterceptors(store());

Modal.setAppElement(document.getElementById('root'));

ReactDOM.render(
    <Provider store={store()}>
        <AppContainer />
    </Provider>,
    document.getElementById('root')
);
