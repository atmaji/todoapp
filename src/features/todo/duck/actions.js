import types from './types';

const requestGetData = () => ({
    type: types.REQUEST_GET_TODO
});

const successGetData = (dataOnProcess, dataFinish, originalData) => ({
    type: types.SUCCESS_GET_TODO,
    dataOnProcess,
    dataFinish,
    originalData
});

const errorGetData = message => ({
    type: types.ERROR_GET_TODO,
    message
});

const deleteGetData = (data) => ({
    type: types.DELETE_GET_TODO,
    data,
})

const createGetData = (dataOnProcess, dataOriginal) => ({
    type: types.CREATE_GET_TODO,
    dataOnProcess,
    dataOriginal,
})

const updateGetData = (dataOnProcess, dataFinished) => ({
    type: types.UPDATE_GET_TODO,
    dataOnProcess,
    dataFinished,
})

export default {
    requestGetData,
    successGetData,
    errorGetData,
    deleteGetData,
    createGetData,
    updateGetData
}