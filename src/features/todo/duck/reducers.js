import types from './types';
import { combineReducers } from 'redux';

const tableReducer = (state = {
    dataOnProcess: [],
    dataOnFinished: [],
    dataOriginal: [],
    message: '',
}, action) => {
    switch (action.type) {
        case types.REQUEST_GET_TODO:
            return state
        case types.SUCCESS_GET_TODO:
            return {
                dataOnProcess: action.dataOnProcess,
                dataOnFinished: action.dataFinish,
                dataOriginal: action.originalData,
                message: '',
            };
        case types.ERROR_GET_TODO:
            return {
                ...state,
                message: action.message,
            };
        case types.DELETE_GET_TODO:
            return {
                ...state,
                dataOnProcess: action.data,
            };
        case types.CREATE_GET_TODO:
            return {
                ...state,
                dataOnProcess: action.dataOnProcess,
                dataOriginal: action.dataOriginal
            };
        case types.UPDATE_GET_TODO:
            return {
                ...state,
                dataOnProcess: action.dataOnProcess,
                dataOnFinished: action.dataFinished
            };
        default:
            return state;
    }
};

const reducers = combineReducers({
    initialTable: tableReducer
});

export default reducers