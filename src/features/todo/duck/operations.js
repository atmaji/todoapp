import actions from './actions';
import api from 'utils/api';
import _ from 'lodash';
import { initialize } from 'redux-form';

const initializeForm = (name, values) => (
    dispatch => {
        dispatch(initialize(name, values));
    }
);

const getInitialTodo = () => (
    dispatch => {
        dispatch(actions.requestGetData())
        return api.get()
            .then(res => {
                const onProcess = res.filter(obj => obj.status === 0);
                const finished = res.filter(obj => obj.status === 1);
                var sOnProcess = _.orderBy(onProcess, ['createdAt'], ['asc'])
                var sFinished = _.orderBy(finished, ['createdAt'], ['dsc'])
                dispatch(actions.successGetData(sOnProcess, sFinished, res));
            }).catch(error => {
                dispatch(actions.errorGetData(error))
            })
    }
)

const deleteData = values => (
    (dispatch, getState) => {
        const state = getState()
        var onProcess = state.todo.initialTable.dataOnProcess.filter(obj => obj.id !== values.id)
        dispatch(actions.deleteGetData(onProcess))
    }
)

const addData = values => (
    (dispatch, getState) => {
        const state = getState();
        var onProcess = state.todo.initialTable.dataOnProcess;
        var dataOriginal = state.todo.initialTable.dataOriginal;
        values = {
            id: dataOriginal.length + 1,
            ...values,
        };
        dataOriginal.push(values)
        onProcess.push(values)
        var sOnProcess = _.orderBy(onProcess, ['createdAt'], ['asc'])
        dispatch(actions.createGetData(sOnProcess, dataOriginal))
    }
)

const updateData = values => (
    (dispatch, getState) => {
        const state = getState();
        var onProcess = state.todo.initialTable.dataOnProcess;
        var finished = state.todo.initialTable.dataOnFinished;
        var sOnProcess = onProcess.filter(row => row.id === values.id).map(obj => ({
            ...obj,
            title: values.title,
            description: values.description,
            status: values.status,
            createdAt: values.createdAt
        }));
        var dltDataOnProcess = onProcess.filter(row => row.id !== values.id);
        var saveOnProcess = sOnProcess[0];
        if (saveOnProcess.status === 0) {
            dltDataOnProcess.push(saveOnProcess)
        } else {
            finished.push(saveOnProcess)
        }
        var dataOrdr = _.orderBy(dltDataOnProcess, ['createdAt'], ['asc'])
        dispatch(actions.updateGetData(dataOrdr, finished))
    }
)

export default {
    getInitialTodo,
    deleteData,
    initializeForm,
    addData,
    updateData
};