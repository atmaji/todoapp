import React, { Fragment, Component } from 'react';
import ReactTable from 'react-table-6';
import { ActionButton, Accordion } from 'components';
import ToDoModalContainer from '../Modal/ToDoModalContainer';
import { Button } from 'reactstrap';


class OnProcessComponent extends Component {
    columns = [{
        Header: 'Id',
        accessor: 'id',
        className: 'text-center'
    }, {
        Header: 'Title',
        accessor: 'title',
        className: 'text-center'
    }, {
        Header: 'Desc',
        accessor: 'description',
        className: 'text-center'
    }, {
        Header: 'Status',
        accessor: 'status',
        className: 'text-center',
        Cell: prop => prop.value === 0? "On Progress" : "Finished"
    }, {
        Header: 'Created At',
        accessor: 'createdAt',
        className: 'text-center'
    }, {
        Header: 'Action',
        className: 'text-center',
        width: 140,
        Cell: prop => {
            let dataUpdate = {
                ...prop.original,
                status: {
                    label: prop.row.status === 0 ? "On Progress" : "Finished",
                    value: prop.row.status
                }
            }
            return (
                <Fragment>
                    <Accordion>
                        {(isOpen, onToggle) => (
                            <Fragment >
                                <ActionButton.UpdateButton onClick={() => { this.props.initializeForm('todomodal', dataUpdate); onToggle() }} />
                                <ToDoModalContainer
                                    onToggle={onToggle}
                                    isOpen={isOpen}
                                    original={prop.original} 
                                    flagProcess="update" />
                            </Fragment>
                        )}
                    </Accordion>
                    <ActionButton.DeleteButton onClick={() => this.props.deleteData(prop.original)} />
                </Fragment>)
        }
    }]

    componentDidMount() {
        this.props.getInitialTodo()
    }

    render() {
        const { tableOnProcess, initializeForm } = this.props
        return (
            <Fragment>
                <Accordion>
                    {(isOpen, onToggle) => (
                        <Fragment >
                            <Button color='primary' onClick={() => { initializeForm('todomodal', undefined); onToggle() }} >Add Data</Button>
                            <ToDoModalContainer
                                onToggle={onToggle}
                                isOpen={isOpen}
                                tableOnProcess={tableOnProcess}
                                flagProcess="create" />
                        </Fragment>
                    )}
                </Accordion>
                <ReactTable
                    columns={this.columns}
                    data={tableOnProcess}
                    minRows={tableOnProcess.length}
                    showPagination={false} />
            </Fragment >
        )
    }
}

export default OnProcessComponent;