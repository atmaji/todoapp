import { connect } from 'react-redux';
import OnProcessComponent from './OnProcessComponent';
import { todoOperations } from '../../duck';
import { reduxForm } from 'redux-form';

const mapStateToProps = state => ({
    tableOnProcess: state.todo.initialTable.dataOnProcess
});

const mapDispatchToProps = {
    deleteData: todoOperations.deleteData,
    initializeForm: todoOperations.initializeForm,
    getInitialTodo: todoOperations.getInitialTodo
};

export default connect(mapStateToProps, mapDispatchToProps)(
    reduxForm({
        form: 'onprocess'
    })(OnProcessComponent)
);