import { FormModal, TextInput, SelectInput } from 'components';
import React from 'react';
import { Field } from 'redux-form';
import moment from 'moment';

const ToDoModalComponent = ({ handleSubmit, onToggle, isOpen, addData, original, tableOnProcess, getInitialTodo, updateData, flagProcess }) => {
    const addOrUpdate = callback => values => {
        values = {
            ...values,
            createdAt: moment(new Date()).format("YYYY-MM-DD HH:mm"),
            status: flagProcess === "create" ? 0 : values.status.value
        }
        if (original && original.id) {
            updateData(values)
        } else {
            addData(values)
        }
        callback()
    }

    const options = [{
        label: 'On Progress',
        value: 0
    }, {
        label: 'Finished',
        value: 1
    }]
    return (
        <FormModal isOpen={isOpen} onSubmit={handleSubmit(addOrUpdate(onToggle))} width="40%" title="Update Data" onToggle={onToggle} isHidden={!isOpen} >
            <Field label="id" name="id" disabled={true} component={TextInput} />
            <Field label="Title" name="title" component={TextInput} />
            <Field label="Description" name="description" component={TextInput} />
            {original && original.id ?
                <Field label="Status" name="status" component={SelectInput} options={options} />
                :
                ''}
            <div>
                <span className="notes"><b>Note</b>: Make sure you have entered the correct data.</span>
            </div>
        </FormModal>
    )
}
export default ToDoModalComponent;