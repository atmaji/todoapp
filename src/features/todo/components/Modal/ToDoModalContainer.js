import { connect } from 'react-redux';
import ToDoModalComponent from './ToDoModalComponent';
import { todoOperations } from '../../duck';
import { reduxForm } from 'redux-form';

const mapStateProps = state => ({

});

const mapDispatchProps = {
    addData: todoOperations.addData,
    updateData: todoOperations.updateData,
    getInitialTodo: todoOperations.getInitialTodo
};

export default connect(mapStateProps, mapDispatchProps)(
    reduxForm({
        form: 'todomodal'
    })(ToDoModalComponent)
);