import { connect } from 'react-redux';
import IndexComponent from './IndexComponent';
import { todoOperations } from '../duck'

const mapStateToProps = state => ({});

const mapDispatchToProps = {
    getInitialTodo: todoOperations.getInitialTodo
};

export default connect(mapStateToProps, mapDispatchToProps)(IndexComponent);