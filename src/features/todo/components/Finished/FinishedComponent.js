import React, { Fragment } from 'react';
import ReactTable from 'react-table-6';

const FinishedComponent = ({ tableFinished }) => {
    const columns = [{
        Header: 'Id',
        accessor: 'id',
        className: 'text-center'
    }, {
        Header: 'Title',
        accessor: 'title',
        className: 'text-center'
    }, {
        Header: 'Desc',
        accessor: 'description',
        className: 'text-center'
    }, {
        Header: 'Status',
        accessor: 'status',
        className: 'text-center',
        Cell: prop => prop.value === 0? "On Progress" : "Finished"
    }, {
        Header: 'Created At',
        accessor: 'createdAt',
        className: 'text-center'
    }]
    return (
        <Fragment>
            <ReactTable columns={columns} data={tableFinished} minRows={tableFinished.length} showPagination={false} />
        </Fragment>
    )
}

export default FinishedComponent;