import { connect } from 'react-redux';
import FinishedComponent from './FinishedComponent';

const mapStateToProps = state => ({
    tableFinished: state.todo.initialTable.dataOnFinished
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(FinishedComponent);