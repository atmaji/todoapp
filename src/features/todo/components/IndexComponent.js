
import React, { Component, Fragment } from 'react';
import TabContainer from './TabContainer'

class IndexComponent extends Component {
    componentDidMount() {
        this.props.getInitialTodo()
    }

    render() {
        return (
            <Fragment>
                <TabContainer />
            </Fragment>
        )
    }
}

export default IndexComponent