
import React, { Component, Fragment } from 'react';
import TodoContainer from 'features/todo'

class AppComponent extends Component {
    render() {
        return (
            <Fragment>
                <TodoContainer />
            </Fragment>
        )
    }
}

export default AppComponent